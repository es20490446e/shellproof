```
USAGE
       shellproof [programPath]

SOURCED
       If the script contains the command "source", shellproof also looks for mistakes in  all
       the other scripts within the same folder.
